function toggleTrace() {
    if (document.getElementById('academicpuma_bibsonomyerror').style.display === 'block') {
        document.getElementById('academicpuma_bibsonomyerror').style.display = 'none';
    } else {
        document.getElementById('academicpuma_bibsonomyerror').style.display = 'block';
    }
}

function bindEvent(element, type, handler) {
    if (element.addEventListener) {
        element.addEventListener(type, handler, false);
    } else {
        element.attachEvent('on' + type, handler);
    }
}

function loadurl(dest, objnev) {

    try {
        xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
        console.log(e);
    }
    xmlhttp.onreadystatechange = function () {
        triggered(objnev);
    };

    xmlhttp.open("GET", dest);
    xmlhttp.send(null);
}

function triggered(objnev) {

    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
        document.getElementById(objnev).innerHTML = xmlhttp.responseText;
    }
}

function removeCitationNumber() {
    // TODO quick fix only so far
    let citationNumberFlag = document.getElementById('tx-extbibsonomy-csl-citation-number');
    if (citationNumberFlag == null) {
        // Remove left entries for the citation numbers
        let cslLeftEntries = document.getElementsByClassName('csl-left-margin');
        while(cslLeftEntries[0]) {
            cslLeftEntries[0].parentNode.removeChild(cslLeftEntries[0]);
        }
        // Fix margin of the actual citations on the right
        let cslRightEntries = document.getElementsByClassName('csl-right-inline');
        for (let i = 0; i < cslRightEntries.length; i++) {
            cslRightEntries[i].style.marginLeft = '0px';
        }
    }
}

function readAuthorLinksContainer() {
    if (document.getElementById('tx-extbibsonomy-csl-author-links-data')) {
        let textContent = document.getElementById('tx-extbibsonomy-csl-author-links-data').textContent.trim();
        let authorsJson = JSON.parse(textContent);
        let csl = '';
        if (document.getElementById('tx-extbibsonomy-csl-author-links-csl')) {
            csl = document.getElementById('tx-extbibsonomy-csl-author-links-csl').textContent.trim()
        }
        generateAuthorLinks(csl, authorsJson);
    }
}

function generateAuthorLinks(csl, authorsJson) {

    let publications = document.getElementsByClassName('csl-entry');
    for (let i = 0; i < publications.length; i++) {
        let authorsWithLinks = '';
        // Check, if CSL used a author span
        if (publications[i].getElementsByClassName('citeproc-author').length > 0) {
            let authors = publications[i].getElementsByClassName('citeproc-author')[0].innerHTML.split(';');
            // Iterate through authors in publication
            for (let j = 0; j < authors.length; j++) {
                let lastname = authors[j].substring(0, authors[j].indexOf(',')).trim();
                let obj = getByLastname(authorsJson.authors, lastname);
                if (obj !== undefined && obj.url != 'none') {
                    authorsWithLinks += '<a target="_blank" href="' + obj.url +
                        '">' + authors[j] + '</a>';
                } else {
                    authorsWithLinks += authors[j];
                }
                authorsWithLinks += '; ';
            }
            publications[i].getElementsByClassName('citeproc-author')[0].innerHTML =
                authorsWithLinks.substring(0, authorsWithLinks.length - 2);
        } else if (csl === 'Springer LNCS') {
            let authors = publications[i].innerHTML.substring(0, publications[i].innerHTML.indexOf(':')).split('., ');
            let tmp = publications[i].innerHTML.substring(publications[i].innerHTML.indexOf(':'));

            for (let j = 0; j < authors.length; j++) {
                if (authors.length > 1 && j < authors.length - 1) {
                    authors[j] = authors[j] + '.';
                }
                let lastname = authors[j].substring(0, authors[j].indexOf(',')).trim();
                let obj = getByLastname(authorsJson.authors, lastname);
                if (obj !== undefined && obj.url != 'none') {
                    authorsWithLinks += '<a target="_blank" href="' + obj.url +
                        '">' + authors[j] + '</a>';
                } else {
                    authorsWithLinks += authors[j];
                }
                authorsWithLinks += ', ';
            }
            publications[i].innerHTML = authorsWithLinks.substring(0, authorsWithLinks.length - 2) + tmp;
        } else {
            // Not a supported CSL used
            break;
        }
    }
}

function initModalLinkListener() {
    let links = document.getElementsByClassName('modal-link');
    for (let i = 0; i < links.length; i++) {
        let link = links[i];
        let id = link.id.substring(link.id.lastIndexOf('-') + 1);
        link.addEventListener('click', function (e) {
            e.preventDefault();
            document.getElementById(this.id.replace('link-', '')).style.display = 'inline-block';
        })
    }
}

function getByLastname(arr, lastname) {

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].lastname === lastname) return arr[i];
    }
}

function initPublicationFilter() {
    if (document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-button").length > 0 && document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-clear").length > 0) {
        document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-button")[0].onclick = filterPublictations;
        document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-clear")[0].onclick = clearPublicationFilter;
    }
}

function filterPublictations() {
    let value = document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-input")[0].value.toLowerCase();
    let items = document.getElementsByClassName("tx-extbibsonomycsl-publication");
    for (let i = 0; i < items.length; i++) {
        let text = items[i].innerText.toLowerCase();
        if (text.includes(value)) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "None";
        }
    }
    let headers = document.getElementsByClassName("tx-extbibsonomy-csl-group");
    for (let i = 0; i < headers.length; i++) {
        headers[i].style.display = "None";
    }
    let anchors = document.getElementsByClassName("tx-extbibsonomycsl-jump-list");
    if (anchors.length > 0) anchors[0].style.display = "None";
}

function clearPublicationFilter() {
    document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-input")[0].value = "";
    let items = document.getElementsByClassName("tx-extbibsonomycsl-publication");
    for (let i = 0; i < items.length; i++) {
        items[i].style.display = "";
    }
    let headers = document.getElementsByClassName("tx-extbibsonomy-csl-group");
    for (let i = 0; i < headers.length; i++) {
        headers[i].style.display = "";
    }
    let anchors = document.getElementsByClassName("tx-extbibsonomycsl-jump-list");
    if (anchors.length > 0) anchors[0].style.display = "";
}

function escapeBibtex() {
    let items = document.getElementsByClassName('tx-extbibsonomycsl-pub-bibtex-content');
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        item.value = item.value.replace(/%/g, '\\%');
        item.value = item.value.replace(/&/g, '\\&');
        item.value = item.value.replace(/#/g, '\\#');
        item.value = item.value.replace(/_/g, '\\_');
        // Remove backslashes of double escaped characters, till a better regex is found
        item.value = item.value.replace(/\\\\/g, '\\');
    }
}

function htmlencodeBibtex() {
    $('.tx-extbibsonomycsl-bibtex-htmlencode').each(function (index) {
        $(this).val(decodeHtml($(this).val()));
    })
}

function decodeHtml(html) {
    let txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

const linkTypes = ['abs', 'bib', 'det', 'end'];

function getLinkTypeId(type, id) {
    return '#' + type + '-' + id;
}

function linkDisplay() {
    $('.tx-extbibsonomycsl-bib-link').each(function (index) {
        $(this).off('click');
        $(this).click(function () {
            let rel = $(this).children(":first").attr('rel');
            // get area (abs|bib|det|end)
            let area = rel.substr(0, 3);
            // get id
            let id = rel.substr(4);
            // hide abstract, bibtex, details, endnote
            for (let j = 0; j < linkTypes.length; j++) {
                let linkType = linkTypes[j];
                if (linkType !== area) {
                    $(getLinkTypeId(linkType, id)).hide();
                }
            }

            // element to show
            $(getLinkTypeId(area, id)).toggle();
        });
    });
}

window.addEventListener("DOMContentLoaded", function () {

    readAuthorLinksContainer();
    linkDisplay();
    initModalLinkListener();
    htmlencodeBibtex();
    escapeBibtex();
    initPublicationFilter();
    removeCitationNumber();

});