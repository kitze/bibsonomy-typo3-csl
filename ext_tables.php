<?php

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tt_content.pi_flexform.extbibsonomycsl_publicationlist.list',
    'EXT:ext_bibsonomy_csl/Configuration/FlexForms/locallang_csh_flexform.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_extbibsonomycsl_domain_model_citationstylesheet',
    'EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_csh_tx_extbibsonomycsl_domain_model_citationstylesheet.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_extbibsonomycsl_domain_model_citationstylesheet',
    'EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_csh_tx_extbibsonomycsl_domain_model_citationstylesheet.xml'
);

if (TYPO3_MODE === 'BE') {
    /**
     * Registers a Backend Module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'ext_bibsonomy_csl',
        'system', // Make module a submodule of 'web'
        'bibsonomybackend', // Submodule key
        'bottom', // Position
        [
            AcademicPuma\ExtBibsonomyCsl\Controller\BackendController::class => 'index, list, create, import, delete, upload, json',
            AcademicPuma\ExtBibsonomyCsl\Controller\BasicAuthController::class => 'list, edit, update, new, create, delete',
            AcademicPuma\ExtBibsonomyCsl\Controller\OAuthController::class => 'list, delete, oauth, callback'
        ], // controller action
        [
            'access' => 'user,group',
            'icon' => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/logo.svg',
            'labels' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_bibsonomybackend.xlf',
        ] // module configuration
    );
}
