<?php
/*
 * Copyright (C) 2015 Sebastian Böttger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * PLUGIN: Publication List
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'ext_bibsonomy_csl',
    'PublicationList',
    'Publication List from PUMA/BibSonomy',
    'ext-bibsonomy-csl-icon'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['extbibsonomycsl_publicationlist'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['extbibsonomycsl_publicationlist'] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'extbibsonomycsl_publicationlist',
    'FILE:EXT:ext_bibsonomy_csl/Configuration/FlexForms/PublicationList.xml'
);

/**
 * PLUGIN: Tag Cloud
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'ext_bibsonomy_csl',
    'TagCloud',
    'Tag Cloud from PUMA/BibSonomy',
    'ext-bibsonomy-csl-icon'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['extbibsonomycsl_publicationdetails'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['extbibsonomycsl_publicationdetails'] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'extbibsonomycsl_tagcloud',
    'FILE:EXT:ext_bibsonomy_csl/Configuration/FlexForms/TagCloud.xml'
);

/**
 * PLUGIN: Publication Details
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'ext_bibsonomy_csl',
    'PublicationDetails',
    'Publication Details from PUMA/BibSonomy',
    'ext-bibsonomy-csl-icon'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['extbibsonomycsl_tagcloud'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['extbibsonomycsl_tagcloud'] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'extbibsonomycsl_publicationdetails',
    'FILE:EXT:ext_bibsonomy_csl/Configuration/FlexForms/PublicationDetails.xml'
);