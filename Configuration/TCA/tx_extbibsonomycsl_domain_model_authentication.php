<?php

defined('TYPO3_MODE') or die();

// $ll = 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => $GLOBALS['TCA']['tx_extbibsonomycsl_domain_model_authentication']['ctrl'],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, host_address, host_user_name, host_api_key, serialized_access_token, enable_o_auth, create_date',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, host_address, host_user_name, host_api_key, serialized_access_token, enable_o_auth, create_date, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [

        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => [
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1],
                    ['LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0]
                ],
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_extbibsonomycsl_domain_model_authentication',
                'foreign_table_where' => 'AND tx_extbibsonomycsl_domain_model_authentication.pid=###CURRENT_PID### AND tx_extbibsonomycsl_domain_model_authentication.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'host_address' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_address',
            'config' => [
                'type' => 'input',
                'size' => 60,
                'eval' => 'trim'
            ],
        ],
        'host_user_name' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_user_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'host_api_key' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_api_key',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'serialized_access_token' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.serialized_access_token',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'enable_o_auth' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.enable_o_auth',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'create_date' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.create_date',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'datetime',
                'checkbox' => 1,
                'default' => time()
            ],
        ],

    ],
];
