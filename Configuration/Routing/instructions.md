# Route Enhancer for pretty URLs
- Open `${path/to/typo3instance}/typo3conf/sites/${your/site}/config.yaml`
- Add the following lines at the end of the file:
```yaml
routeEnhancers:
  BibsonomyPlugin:
    type: Extbase
    extension: Extbibsonomycsl
    plugin: Publicationlist
    routes:
      - { routePath: '/preview/{preview}/{userName}/{intraHash}/{fileName}', _controller: 'Document::view'}
      - { routePath: '/publication/{userName}/{intraHash}/{fileName}', _controller: 'Document::download'}
    defaultController: 'Document::view'
    defaults:
      page: '0'
    requirements:
      page: \d+
```
If a `routeEnhancers` entry already exists, simply add the routing for the BibSonomy plugin above.

# Remove cHash for BibSonomy plugin parameters

- Open `${path/to/typo3instance}/typo3conf/LocalConfiguration.php`
- Search for `'FE'`
- Add the following lines to the entry:
```php
'cacheHash' => [
    'excludedParameters' => [
        'tx_extbibsonomycsl_publicationlist[action]',
        'tx_extbibsonomycsl_publicationlist[preview]',
        'tx_extbibsonomycsl_publicationlist[controller]',
        'tx_extbibsonomycsl_publicationlist[userName]',
        'tx_extbibsonomycsl_publicationlist[fileName]',
        'tx_extbibsonomycsl_publicationlist[intraHash]',
    ],
],
```
If a similiar list already exists, just add the list of parameters to the already existing list.