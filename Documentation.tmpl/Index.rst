﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
PUMA/BibSonomy CSL
=============================================================

.. only:: html

	:Classification:
    		ext_bibsonomy_csl

    	:Version:
    		|release|

	:Language:
    		en

    	:Description:
    		Create advanced bibliographies or simple lists of publications and tag clouds from your PUMA or BibSonomy account. Style your publication lists using CSL stylesheets (http://citationstyles.org).

	:Keywords:
    		comma,separated,list,of,keywords

    	:Copyright:
    		2012-2015

    	:Author:
    		Sebastian Böttger

    	:Email:
    		boettger@cs.uni-kassel.de

    	:License:
    		This document is published under the Open Content License
    		available from http://www.opencontent.org/opl.shtml

	:Rendered:
    		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
    	:glob:

    	Introduction/Index
    	User/Index
    	Administrator/Index
    	Configuration/Index
    	Developer/Index
    	KnownProblems/Index
    	ToDoList/Index
    	ChangeLog/Index
    	Targets
