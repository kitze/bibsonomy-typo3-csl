<?php

defined('TYPO3_MODE') or die();

if (TYPO3_MODE === 'BE') {
    $icons = [
        'ext-bibsonomy-csl-icon' => 'logo.svg',
    ];
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    foreach ($icons as $identifier => $path) {
        if (!$iconRegistry->isRegistered($identifier)) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:ext_bibsonomy_csl/Resources/Public/Icons/' . $path]
            );
        }
    }
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'ext_bibsonomy_csl', // extension name
    'PublicationList', // plugin name
    [
        AcademicPuma\ExtBibsonomyCsl\Controller\PublicationController::class => 'list, details',
        AcademicPuma\ExtBibsonomyCsl\Controller\DocumentController::class => 'download, preview, view'
    ], // controller actions
    [], // noncacheable controller
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'ext_bibsonomy_csl', // extension name
    'TagCloud', // plugin name
    [
        AcademicPuma\ExtBibsonomyCsl\Controller\TagController::class => 'list'
    ], // controller actions
    [], // noncacheable controller
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    @import \'EXT:ext_bibsonomy_csl/Configuration/TSConfig/ContentElement.tsconfig\'
');