<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Model;


/**
 * CitationStylesheet
 */
class CitationStylesheet extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * id
     *
     * @var string
     */
    protected $id = '';

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * xmlSource
     *
     * @var string
     */
    protected $xmlSource = '';

    /**
     * Returns the id
     *
     * @return string $id
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Sets the id
     *
     * @param string $id
     *
     * @return void
     */
    public function setId($id)
    {

        $this->id = $id;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {

        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {

        $this->title = $title;
    }

    /**
     * Returns the xmlSource
     *
     * @return string $xmlSource
     */
    public function getXmlSource()
    {

        return $this->xmlSource;
    }

    /**
     * Sets the xmlSource
     *
     * @param string $xmlSource
     *
     * @return void
     */
    public function setXmlSource($xmlSource)
    {

        $this->xmlSource = $xmlSource;
    }

}