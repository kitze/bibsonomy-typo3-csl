<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib\Storage;

use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

/**
 * Short description
 *
 * @since 11/09/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class FeSessionStorage extends AbstractSessionStorage
{

    /**
     * Read session data
     *
     * @param string $key
     * @param string $type
     *
     * @return mixed
     */
    public function read($key, $type = 'ses')
    {

        $sessionData = $this->getFeUser()->getKey($type, $this->getKey($key));
        if ($sessionData == '') {
            return '';
        }

        return $sessionData;
    }

    /**
     * Has some key or not
     *
     * @param string $key
     * @param string $type
     *
     * @return bool
     */
    public function has($key, $type = 'ses')
    {

        $sessionData = $this->getFeUser()->getKey($type, $this->getKey($key));
        if ($sessionData == '') {
            return false;
        }

        return true;
    }

    /**
     * Write data to session
     *
     * @param string $key
     * @param mixed $data
     *
     * @return void
     */
    public function write($key, $data, $type = 'ses')
    {

        $this->getFeUser()->setKey($type, $this->getKey($key), $data);
        $this->getFeUser()->storeSessionData();
    }

    /**
     * Remove data from session
     *
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = 'ses')
    {

        if ($this->has($key, $type)) {
            $this->write($key, null, $type);
        }
    }

    /**
     *
     * @param string $type
     */
    protected function setUserDataChanged($type = 'ses')
    {

        switch ($type) {
            case 'ses':
                $this->getFeUser()->sesData_change = 1;
                break;
            case 'user':
                $this->getFeUser()->userData_change = 1;
                break;
            default:
                $this->getFeUser()->sesData_change = 1;
                break;
        }
    }

    /**
     *
     * @return FrontendUserAuthentication
     */
    protected function getFeUser()
    {

        return $GLOBALS['TSFE']->fe_user;
    }

    /**
     *
     * @return FrontendUserAuthentication
     */
    public function getUser()
    {

        return $this->getFeUser();
    }

}