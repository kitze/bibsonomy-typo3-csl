<?php


namespace AcademicPuma\ExtBibsonomyCsl\Lib;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

class TYPO3Utils
{

    public static function getRelExtentionPath(string $extensionKey)
    {
        return PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($extensionKey));
    }

}