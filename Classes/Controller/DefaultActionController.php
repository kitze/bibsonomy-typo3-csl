<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthenticationException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository;
use AcademicPuma\RestClient\Accessor\BasicAuthAccessor;
use AcademicPuma\RestClient\Accessor\OAuthAccessor;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;

/**
 * Abstract Action Controller to handle errors/exceptions by using custom view
 * to display errors.
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class DefaultActionController extends ActionController
{
    protected $host;

    /**
     * Custom error view
     *
     * @param \Exception $e
     */
    protected function handleError(\Exception $e)
    {
        $response = new Response();
        $controllerContext = $this->buildControllerContext();
        $controllerContext->getRequest()->setControllerName('Default');
        $controllerContext->getRequest()->setControllerActionName('error');
        $this->view->setControllerContext($controllerContext);
        $content = $this->view->assign('exception', $e)->render('error');
        // $response->addAdditionalHeaderData('<script type="text/javascript" src="' . ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/tx_extbibsonomycsl.js" /></script>');

        return $response->withBody($content);
    }

    /**
     * @override
     */
    protected function callActionMethod(RequestInterface $request): ResponseInterface
    {
        $response = new Response();
        try {
            $response = parent::callActionMethod($request);
        } catch (\Exception $e) {
            $response = $this->handleError($e);
        }

        return $response;
    }

    /**
     * @return BasicAuthAccessor|OAuthAccessor
     * @throws AuthenticationException
     */
    public function makeAccessor()
    {
        $host = '';
        $apiUser = $apiKey = '';
        $basicAuth = true;

        $accessor = null;

        switch ($this->settings['authconfig']) {

            case 'beauth':

                /** @var AuthenticationRepository $authenticationRepository */
                $authenticationRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\AuthenticationRepository');

                /** @var Authentication $auth */
                $auth = $authenticationRepository->findByUid($this->settings['authentication']);

                if (empty($auth)) {
                    throw new AuthenticationException("Could not find valid API credentials. Please check the plugin settings!");
                }

                $host = $auth->getHostAddress();
                if (!$auth->isEnableOAuth()) {
                    $apiUser = $auth->getHostUserName();
                    $apiKey = $auth->getHostApiKey();

                } else {
                    $confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl']);
                    $basicAuth = false;
                    $accessor = new OAuthAccessor($host, unserialize($auth->getSerializedAccessToken()),
                        $confArray['oauthConsumerToken'], $confArray['oauthConsumerSecret']);
                }

                break;
            case 'custom':
                $apiUser = $this->settings['bib_login_name'];
                $apiKey = $this->settings['bib_api_key'];
                $host = $this->settings['bib_server'];
        }

        if ($basicAuth) {
            $accessor = new BasicAuthAccessor($host, $apiUser, $apiKey);
        }

        $this->host = $host;

        return $accessor;
    }

    protected function getHost()
    {
        return $this->getHost();
    }

}
