<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\InvalidStylesheetException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use AcademicPuma\ExtBibsonomyCsl\Log\Logger;
use AcademicPuma\RestClient\Exceptions\FileNotFoundException;
use AcademicPuma\RestClient\Renderer\XMLModelUnserializer;
use DOMException;
use ReflectionException;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\File\ExtendedFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;

/**
 * Controller for backend actions
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 */
class BackendController extends DefaultActionController
{

    protected $pageId;

    /**
     * Repository for Citation Stylesheets
     *
     * @var CitationStylesheetRepository $citationStylesheetRepository ;
     */
    protected CitationStylesheetRepository $citationStylesheetRepository;

    /**
     * @return void
     */
    protected function initializeAction()
    {
        $this->pageId = intval(GeneralUtility::_GP('id'));
    }

    /**
     * Inject the citation stylesheet repository
     *
     * @param CitationStylesheetRepository $citationStylesheetRepository
     */
    public function injectCitationStylesheetRepository(CitationStylesheetRepository $citationStylesheetRepository)
    {
        $this->citationStylesheetRepository = $citationStylesheetRepository;
    }

    public function indexAction()
    {

        //echo "<pre>";
        //print_r($GLOBALS['TBE_STYLES']['spriteIconApi']['iconsAvailable']);
        //echo "</pre>";
    }

    /**
     * assigns all stylesheets in view
     *
     * @return void
     * @throws DOMException
     * @throws ReflectionException
     */
    public function listAction()
    {

        $result = $this->citationStylesheetRepository->findAllOfBEUser();
        $this->view->assign('styles', $result);
        $path = ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . "/Resources/Private/Posts/einstein.xml";
        if (!file_exists($path)) {
            $logger = Logger::getLogger(__CLASS__);
            $logger->error('File does not exist ' . $path);
        }
        $post = file_get_contents($path);
        $modelUnserializer = new XMLModelUnserializer($post);
        $post = $modelUnserializer->convertToModel();
        $this->view->assign('post', $post);
    }

    /**
     * adds a new stylesheet to the repository
     *
     * @param string $xmlSource
     *
     * @return void
     */
    public function createAction(string $xmlSource = '')
    {

        if ($this->request->hasArgument('xmlSource')) {
            $xmlSource = $this->request->getArgument('xmlSource');

            try {
                $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
                try {
                    $this->citationStylesheetRepository->add($stylesheet);
                } catch (RepositoryException $e) {
                    $this->addFlashMessage($e->getMessage(),'ERROR', AbstractMessage::ERROR);

                    return;
                }
                $this->addFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been saved.', 'Success');
            } catch (\Exception $e) {
                $exceptionDescription = 'Exception in ' . $e->getFile() . ' on line ' . $e->getLine();
                $this->addFlashMessage($e->getMessage() . "<br />$exceptionDescription", 'Error',AbstractMessage::ERROR);
            }
        }
    }

    /**
     * imports a new stylesheet from the given url and adds it to the repository
     *
     * @param string $stylePath
     *
     * @throws FileNotFoundException
     * @throws InvalidStylesheetException
     * @throws NoSuchArgumentException
     * @throws StopActionException
     */
    public function importAction(string $stylePath = "")
    {

        if ($this->request->hasArgument('stylePath')) {

            $path = $this->request->getArgument('stylePath');

            if (!file_exists($path)) {
                throw new FileNotFoundException($path);
            }

            $xmlSource = file_get_contents($path);
            $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
            try {
                $this->citationStylesheetRepository->add($stylesheet);
            } catch (RepositoryException $e) {
                $this->addFlashMessage($e->getMessage(), 'ERROR', FlashMessage::ERROR);

                return;
            }
            $this->addFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been imported.', 'Success');

        } else {
            $this->addFlashMessage('Missing file name.', 'Error', AbstractMessage::ERROR);
        }

        $this->redirect('list');

    }

    /**
     * @param CitationStylesheet $stylesheet
     *
     * @return void
     * @todo implement when needed
     *
     */
    public function editAction(CitationStylesheet $stylesheet)
    {
        //TODO: implement
    }

    /**
     * @param CitationStylesheet $stylesheet
     *
     * @return void
     * @todo implement when needed
     *
     */
    public function updateAction(CitationStylesheet $stylesheet)
    {
        //TODO: implement
    }

    /**
     * deletes the given stylesheet
     *
     * @param CitationStylesheet $stylesheet
     *
     * @throws IllegalObjectTypeException
     * @throws StopActionException
     */
    public function deleteAction(CitationStylesheet $stylesheet)
    {

        $this->citationStylesheetRepository->remove($stylesheet);
        $this->addFlashMessage('The stylesheet "' . $stylesheet->getTitle() . '" has been removed.',
            'Stylesheet deleted', AbstractMessage::OK);
        $this->redirect('list');
    }

    /**
     * @throws InvalidStylesheetException
     * @throws NoSuchArgumentException
     */
    public function uploadAction()
    {

        if ($this->request->hasArgument('file')) {

            $file = $this->request->getArgument('file');

            if ($file) {
                $fileUtility = GeneralUtility::makeInstance(ExtendedFileUtility::class);
                $fileName = $fileUtility->getUniqueName($file['name'],
                    GeneralUtility::getFileAbsFileName('uploads/'), 1);

                GeneralUtility::upload_copy_move($file['tmp_name'], $fileName);
            } else {
                $this->addFlashMessage('Upload has failed.', 'ERROR', AbstractMessage::ERROR);

                return;
            }

            $xmlSource = Helper::getDataFromCSLFile($fileName);

            $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
            try {
                $this->citationStylesheetRepository->add($stylesheet);
            } catch (RepositoryException $e) {

                $this->addFlashMessage($e->getMessage(), 'ERROR', AbstractMessage::ERROR);

                return;
            }
            $this->addFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been added.', 'Success');

        }
    }

    public function jsonAction()
    {

        $qry = filter_input(INPUT_GET, 'query', FILTER_SANITIZE_STRING);
        $styles = $this->citationStylesheetRepository->findAllStylesJsonByQuery($qry);
        header('Content-Type: application/json');
        print json_encode($styles);
        die;
    }

}

